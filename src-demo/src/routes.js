// in src/Routes.js
import * as React from "react";
// tslint:disable-next-line:no-var-requires
import { ColorField, ColorInput } from 'react-admin-color-input';
import { makeStyles } from '@material-ui/core/styles';
import {
  Datagrid,
  List,
  Show,
  Create,
  Edit,
  Filter,
  SimpleShowLayout,
  SimpleForm,
  TextField,
  TextInput,
  ShowButton,
  EditButton,
  DeleteButton,  
  TabbedShowLayout, 
  Tab,
  TabbedForm,
  FormTab,
  ArrayInput,
  SimpleFormIterator

} from "react-admin";
import { withStyles } from '@material-ui/core/styles';
import MyMap from './MyMap';
import { MyMapField } from './MyMapComponent';
// const useStyles = makeStyles({
//     priceHeader: { height: 50 },
// });

// const MyColorInput = props => {
//     const classes = useStyles();
//     return <ColorInput headerClassName={classes.priceHeader} {...props} />;
// }

const RouteFilter = props => (
  <Filter {...props}>
    <TextInput label="Search" source="title" alwaysOn />
  </Filter>
);

export const RouteList = props => (
  <List
    {...props}
    filters={<RouteFilter />}
    //filter={{ updatedby: "test@example.com" }}
  >
    <Datagrid>
      {/* <TextField source="id" /> */}
      <TextField source="name" />
      <ColorField source="color" />      
      <ShowButton label="" />
      <EditButton label="" />
      <DeleteButton label="" redirect={false} />
    </Datagrid>
  </List>
);

export const RouteShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
        <TextField source="id" />        
        <TextField source="name" />      
        <TextField source="recorrido" />      
        <ColorField source="color" />
    </SimpleShowLayout>
  </Show>
);

export const RouteCreate = props => (
  <Create {...props}>
    <SimpleForm>
        <TextInput source="id" validation={{ required: true }}/>        
        <TextInput source="name" validation={{ required: true }}/>
        <TextInput source="recorrido" />
        <ColorInput source="color" validation={{ required: true }}/>        
    </SimpleForm>
  </Create>
);

export const RouteEdit = props => (
  <Edit undoable={true} {...props}>      
    <TabbedForm>
        <FormTab label="Datos">            
            <TextInput disabled source="id" validation={{ required: true }}/>
            <TextInput source="name" validation={{ required: true }}/>
            <TextInput source="recorrido" />
            <ColorInput source="color" validation={{ required: true }}/>
        </FormTab>
        <FormTab label="Mapa">
            <MyMapField 
              label="algo" 
              source="coordinates" 
              style={{
                position: 'relative',    
                width: '70%',
                height: '60vh',
              }}
            />
            {/* <ArrayInput source="coordinates">
                    <SimpleFormIterator>
                        <TextInput source="latitude" />
                        <TextInput source="longitude" />  
                    </SimpleFormIterator>
                </ArrayInput>      */}
        </FormTab>
    </TabbedForm>
  </Edit>
);
