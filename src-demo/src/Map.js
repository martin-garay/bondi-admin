import React, { Component } from 'react';
import GoogleMapReact,{PolyLine} from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

const config = {
    center: {
        lat: -34.566796,      
        lng: -59.115170
    },
    zoom: 11
};

const Map = (props) => {    

    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyChiPmjq-hr2QNpsWyhOxnv2i0anEyzN2U" }}
          defaultCenter={config.center}
          defaultZoom={config.zoom}
        >            
          {/* <PolyLine
            path={this.props.coordinates}
            strokeColor={this.props.color}
            strokeWidth={3}
          /> */}
          <AnyReactComponent
            lat={-34.566796}
            lng={-59.115170}
            text="My Marker"
          />
        </GoogleMapReact>
      </div>
    );
  
}

export default Map;