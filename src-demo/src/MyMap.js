import React, { Component } from 'react';
import { Map, GoogleApiWrapper, Marker, Polyline } from 'google-maps-react';

class MyMap extends Component {
    constructor(props) {
      super(props);
      console.log(props);
      
      this.state = {
        selected: null,
        coordinates: this.transformCoordinates()
      }
    }
    transformCoordinates = () => {
      const pathPolyline = [];
      this.props.coordinates.map( point => {
        pathPolyline.push({lat: point.latitude, lng: point.longitude});
      })
      console.log(pathPolyline);
      return pathPolyline;
    }
    handlerOnClick = (index) => {
      this.setState({selected: index});
    }
    handleOnDelete = (index) => {

    }
    handleAddPoint = (lat,lng) => {
      this.setState();
    }

    onMarkerDragEnd = (coord, index) => {
      const { latLng } = coord;
      const lat = latLng.lat();
      const lng = latLng.lng();
        
      let coordinates = [...this.state.coordinates];      
      let coordinate = {...coordinates[index]};
      coordinate.lat = lat;
      coordinate.lng = lng;
      coordinates[index] = coordinate;
      this.setState({coordinates});
    };
  
    render() {
      
      return (
        <div style={{ height: '60vh', width: '60%' }}>
          <Map
            google={this.props.google}
            zoom={8}
            style={mapStyles}
            initialCenter={{ lat: -34.566796, lng: -59.115170}}
          >
            {this.state.coordinates.map(({lat,lng}, index) => {
              const isSelected = index == this.state.selected;
              return <Marker
                        key={index} 
                        id={index}
                        name={index}
                        draggable={true}
                        animation={isSelected ? this.props.google.maps.Animation.BOUNCE : null} 
                        defaultAnimation={this.props.google.maps.Animation.DROP}                        
                        position={{ 
                          lat,
                          lng 
                        }}
                        onClick={() => this.handlerOnClick(index)} 
                        onDragend={(t, map, coord) => this.onMarkerDragEnd(coord, index)}
                      />
              })
            }
            <Polyline 
              path={this.state.coordinates}              
              options={{ 
              strokeColor: '#00ffff',
              strokeOpacity: 1,
              strokeWeight: 2,
              icons: [{ 
                icon: "hello",
                offset: '0',
                repeat: '10px'
              }],
            }}
            />
          </Map>
          </div>
      );
    }
  }

  const mapStyles = {
    position: 'relative',    
    width: '70%',
    height: '60vh',
  };

  export default GoogleApiWrapper({
    apiKey: "AIzaSyChiPmjq-hr2QNpsWyhOxnv2i0anEyzN2U"
  })(MyMap);
  
  